//
//  EnterpriseInfoViewController.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 24/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

import UIKit

class EnterpriseInfoViewController: UIViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    lazy var enterpriseInfoView = EnterpriseInfoView()
    var viewModel: EnterpriseInfoViewModel
    
    init(viewModel: EnterpriseInfoViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view = enterpriseInfoView
        enterpriseInfoView.navigationBar.buttonAction = backButtonTapped(_:)
        viewModel.populate(enterpriseInfoView)
    }
    
    @objc func backButtonTapped(_ button: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    required init?(coder: NSCoder) {
        fatalError("An enterprise should be passed to this controller")
    }

}
