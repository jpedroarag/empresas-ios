//
//  EnterprisesViewController.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 24/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

import UIKit

class EnterprisesViewController: UIViewController {
    
    lazy var keyboardHandler = KeyboardHandler(view: self.view)
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    lazy var enterprisesView = EnterprisesView()
    lazy var viewModel = EnterprisesViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        view = enterprisesView
        configureTableView(enterprisesView.enterprisesTableView)
        configureKeyboardHandler()
        configureBindings()
        fetchData()
    }
    
    func configureTableView(_ tableView: UITableView) {
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func configureKeyboardHandler() {
        keyboardHandler.willShow = {
            self.enterprisesView.setHeaderViewHeightMultiplier(0.15)
            self.enterprisesView.layoutSubviews()
        }
        keyboardHandler.willHide = {
            self.enterprisesView.setHeaderViewHeightMultiplier(0.3)
            self.enterprisesView.layoutSubviews()
        }
    }
    
    func configureBindings() {
        viewModel.visibleEnterprises.bind { enterprises in
            DispatchQueue.main.async {
                self.enterprisesView.loadingIndicatorView.stopAnimating()
                self.enterprisesView.enterprisesTableView.reloadData()
            }
        }
        enterprisesView.headerView.searchTextField.delegate = viewModel
    }
    
    func fetchData() {
        enterprisesView.loadingIndicatorView.startAnimating()
        viewModel.fetchEnterprises()
    }

}

extension EnterprisesViewController: UITableViewDataSource, UITableViewDelegate {
    
    // MARK: Count stuff
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.visibleEnterprises.value.count
    }
    
    // MARK: Creation stuff
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "results") as? ResultsFoundTableViewHeader
        let numberOfResults = String(format: "%.2d", viewModel.visibleEnterprises.value.count)
        headerView?.label.text = "\(numberOfResults) resultados encontrados"
        headerView?.label.isHidden = !viewModel.isSearching
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "enterprise", for: indexPath)
            as? EnterpriseTableViewCell else {
                return UITableViewCell()
        }
        let color = getPlaceholderColor(for: indexPath.row)
        let name = viewModel.visibleEnterprises.value[indexPath.row].enterpriseName
        cell.enterpriseImageView.backgroundColor = color
        cell.enterpriseImageView.label.text = name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let tappedEnterprise = viewModel.visibleEnterprises.value[indexPath.row]
        let infoViewModel = EnterpriseInfoViewModel(enterprise: tappedEnterprise)
        let infoController = EnterpriseInfoViewController(viewModel: infoViewModel)
        
        let color = getPlaceholderColor(for: indexPath.row)
        infoController.enterpriseInfoView.imageView.backgroundColor = color
        
        navigationController?.pushViewController(infoController, animated: true)
    }
    
    private func getPlaceholderColor(for row: Int) -> UIColor? {
        let placeholderBackgrounds: [UIColor?] = [.placeholderBlue,
                                                  .placeholderRed,
                                                  .placeholderGreen]
        let backgroundIndex = row % placeholderBackgrounds.count
        return placeholderBackgrounds[backgroundIndex]
    }
    
    // MARK: Height stuff
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let searchTextFieldHeight: CGFloat = 48
        if !viewModel.isSearching {
            return searchTextFieldHeight/2 + 16
        }
        return searchTextFieldHeight/2 + searchTextFieldHeight
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 128
    }
    
}
