//
//  NavigationController.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 24/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

import UIKit

class NavigationController: UINavigationController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if let topController = topViewController,
            topController.isKind(of: EnterpriseInfoViewController.self) {
            return .default
        }
        return .lightContent
    }

}
