//
//  LoginViewController.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 23/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    lazy var keyboardHandler = KeyboardHandler(view: loginView)
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    lazy var loginView = LoginView()
    lazy var viewModel = LoginViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view = loginView
        configureKeyboardHandler()
        configureBindings()
    }
    
    func configureKeyboardHandler() {
        keyboardHandler.willShow = {
            self.loginView.headerView.collapse()
            self.loginView.setHeaderViewHeightMultiplier(0.2)
            self.loginView.layoutSubviews()
        }
        keyboardHandler.willHide = {
            self.loginView.headerView.expand()
            self.loginView.setHeaderViewHeightMultiplier(0.4)
            self.loginView.layoutSubviews()
        }
    }
    
    func configureBindings() {
        viewModel.isLoggedIn.bind { success in
            success ? self.succededLogin() : self.failedLogin()
        }
        loginView.buttonAction = loginButtonTapped(_:)
        loginView.inputsView.passwordTextFieldReturnAction = { _ in
            self.loginButtonTapped(self.loginView.loginButton)
        }
    }
    
    private func succededLogin() {
        DispatchQueue.main.async {
            self.loginView.loadingIndicatorView.stopAnimating()
            self.presentNavigationController(root: EnterprisesViewController())
        }
    }
    
    private func failedLogin() {
        DispatchQueue.main.async {
            self.loginView.loadingIndicatorView.stopAnimating()
            self.loginView.inputsView.addIncorrectCredentialsOverlays()
        }
    }
    
    func loginButtonTapped(_ button: UIButton) {
        loginView.loadingIndicatorView.startAnimating()
        let email = loginView.inputsView.emailTextField.text!
        let password = loginView.inputsView.passwordTextField.text!
        viewModel.login(email: email, password: password)
    }
    
    func presentNavigationController(root: UIViewController) {
        let navigation = NavigationController(rootViewController: root)
        navigation.navigationBar.isHidden = true
        navigation.navigationBar.prefersLargeTitles = false
        navigation.modalPresentationStyle = .fullScreen
        self.present(navigation, animated: true, completion: nil)
    }
    
}


