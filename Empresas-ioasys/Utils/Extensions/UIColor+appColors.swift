//
//  UIColor+appColors.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 23/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

import UIKit

extension UIColor {
    
    static let appPink = UIColor(named: "AppPink")
    static let appGray = UIColor(named: "AppGray")
    static let appWhite = UIColor(named: "AppWhite")
    static let appRed = UIColor(named: "AppRed")
    
    static let placeholderRed = UIColor(named: "PlaceholderRed")
    static let placeholderGreen = UIColor(named: "PlaceholderGreen")
    static let placeholderBlue = UIColor(named: "PlaceholderBlue")
    
}
