//
//  UILabel+initWithFont.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 23/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

import UIKit

extension UILabel {
    
    convenience init(withFont font: UIFont?) {
        self.init()
        self.font = font
    }
    
}
