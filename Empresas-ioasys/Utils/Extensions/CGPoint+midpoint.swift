//
//  CGPoint+midpoint.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 25/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

import CoreGraphics

extension CGPoint {
    static func midpoint(left: CGPoint, right: CGPoint) -> CGPoint {
        var newPoint = CGPoint.zero
        newPoint.x = (left.x + right.x) / 2;
        newPoint.y = (left.y + right.y) / 2;
        return newPoint
    }
}
