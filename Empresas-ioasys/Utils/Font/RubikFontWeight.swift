//
//  RubikFontWeight.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 23/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

import Foundation

enum RubikFontWeight: String {
    case light = "Light"
    case medium = "Medium"
    case regular = "Regular"
    case bold = "Bold"
}
