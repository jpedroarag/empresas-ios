//
//  KeyboardHandler.swift
//  ArcTouchTest
//
//  Created by João Pedro Aragão on 28/12/19.
//  Copyright © 2019 João Pedro Aragão. All rights reserved.
//

import UIKit

class KeyboardHandler {
    
    typealias Block = () -> Void
    
    var view: UIView
    var keyboardHeight: CGFloat!
    var willShow: Block?
    var willHide: Block?
    
    init(view: UIView) {
        self.view = view
        self.addKeyboardObservers()
    }
    
    deinit {
        self.removeKeyboardObservers()
    }
    
    func addKeyboardObservers() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(sender:)),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil);
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(sender:)),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil);
    }
    
    func removeKeyboardObservers() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(sender: NSNotification) {
        if let keyboardFrame = sender.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            keyboardHeight = keyboardFrame.cgRectValue.height
            willShow?()
        }
    }

    @objc func keyboardWillHide(sender: NSNotification) {
        willHide?()
    }
    
}
