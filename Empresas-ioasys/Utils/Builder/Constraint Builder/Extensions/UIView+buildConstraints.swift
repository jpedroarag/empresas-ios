//
//  UIView+makeConstraints.swift
//  
//
//  Created by João Pedro Aragão on 27/12/19.
//  Copyright © 2019 João Pedro Aragão. All rights reserved.
//

import UIKit

extension UIView {
    
    func makeConstraints(block: (ConstraintBuilder) -> Void) {
        translatesAutoresizingMaskIntoConstraints = false
        let builder = ConstraintBuilder(topAnchor: topAnchor,
                                        leftAnchor: leftAnchor,
                                        rightAnchor: rightAnchor,
                                        bottomAnchor: bottomAnchor,
                                        centerYAnchor: centerYAnchor,
                                        centerXAnchor: centerXAnchor,
                                        heightAnchor: heightAnchor,
                                        widthAnchor: widthAnchor)
        block(builder)
    }
    
}
