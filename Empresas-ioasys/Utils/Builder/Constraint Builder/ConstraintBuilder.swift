//
//  ConstraintBuilder.swift
//  
//
//  Created by João Pedro Aragão on 27/12/19.
//  Copyright © 2019 João Pedro Aragão. All rights reserved.
//

import UIKit

typealias HorizontalConstraintItem = LayoutConstraintItem<NSLayoutXAxisAnchor>
typealias VerticalConstraintItem = LayoutConstraintItem<NSLayoutYAxisAnchor>

struct ConstraintBuilder {
    
    var top: VerticalConstraintItem
    var left: HorizontalConstraintItem
    var right: HorizontalConstraintItem
    var bottom: VerticalConstraintItem
    
    var centerY: VerticalConstraintItem
    var centerX: HorizontalConstraintItem
    
    var height: DimensionConstraintItem
    var width: DimensionConstraintItem
    
    init(topAnchor: NSLayoutAnchor<NSLayoutYAxisAnchor>,
         leftAnchor: NSLayoutAnchor<NSLayoutXAxisAnchor>,
         rightAnchor: NSLayoutAnchor<NSLayoutXAxisAnchor>,
         bottomAnchor: NSLayoutAnchor<NSLayoutYAxisAnchor>,
         centerYAnchor: NSLayoutAnchor<NSLayoutYAxisAnchor>,
         centerXAnchor: NSLayoutAnchor<NSLayoutXAxisAnchor>,
         heightAnchor: NSLayoutDimension,
         widthAnchor: NSLayoutDimension) {
        top = LayoutConstraintItem(anchor: topAnchor)
        left = LayoutConstraintItem(anchor: leftAnchor)
        right = LayoutConstraintItem(anchor: rightAnchor)
        bottom = LayoutConstraintItem(anchor: bottomAnchor)
        centerY = LayoutConstraintItem(anchor: centerYAnchor)
        centerX = LayoutConstraintItem(anchor: centerXAnchor)
        height = DimensionConstraintItem(anchor: heightAnchor)
        width = DimensionConstraintItem(anchor: widthAnchor)
    }
    
    init(layoutGuide: UILayoutGuide) {
        top = LayoutConstraintItem(anchor: layoutGuide.topAnchor)
        left = LayoutConstraintItem(anchor: layoutGuide.leftAnchor)
        right = LayoutConstraintItem(anchor: layoutGuide.rightAnchor)
        bottom = LayoutConstraintItem(anchor: layoutGuide.bottomAnchor)
        centerY = LayoutConstraintItem(anchor: layoutGuide.centerYAnchor)
        centerX = LayoutConstraintItem(anchor: layoutGuide.centerXAnchor)
        height = DimensionConstraintItem(anchor: layoutGuide.heightAnchor)
        width = DimensionConstraintItem(anchor: layoutGuide.widthAnchor)
    }
    
    func fill(onView: UIView, _ paddings: [Paddings] = []) {
        var topPadding: CGFloat = 0
        var leftPadding: CGFloat = 0
        var rightPadding: CGFloat = 0
        var bottomPadding: CGFloat = 0
        
        paddings.forEach { padding in
            switch padding {
            case .top(let value):
                topPadding = value
            case .left(let value):
                leftPadding = value
            case .right(let value):
                rightPadding = value
            case .bottom(let value):
                bottomPadding = value
            }
        }
        
        top.constraint(toAnchor: onView.topAnchor, padding: topPadding)
        left.constraint(toAnchor: onView.leftAnchor, padding: leftPadding)
        right.constraint(toAnchor: onView.rightAnchor, padding: rightPadding)
        bottom.constraint(toAnchor: onView.bottomAnchor, padding: bottomPadding)
    }
    
}

enum Paddings: Equatable {
    case top(CGFloat)
    case left(CGFloat)
    case right(CGFloat)
    case bottom(CGFloat)
}
