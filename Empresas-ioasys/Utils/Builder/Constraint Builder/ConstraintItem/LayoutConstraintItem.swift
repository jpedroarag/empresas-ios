//
//  LayoutConstraintItem.swift
//  
//
//  Created by João Pedro Aragão on 27/12/19.
//  Copyright © 2019 João Pedro Aragão. All rights reserved.
//

import UIKit

struct LayoutConstraintItem<T> where T: AnyObject {
    
    let anchor: NSLayoutAnchor<T>
    
    @discardableResult
    func constraint(toAnchor: NSLayoutAnchor<T>,
                    padding: CGFloat = 0,
                    relation: NSLayoutConstraint.Relation = .equal,
                    priority: Float = 1000) -> NSLayoutConstraint {
        var constraint: NSLayoutConstraint
        switch relation {
        case .equal:
            constraint = anchor.constraint(equalTo: toAnchor, constant: padding)
        case .greaterThanOrEqual:
            constraint = anchor.constraint(greaterThanOrEqualTo: toAnchor, constant: padding)
        case .lessThanOrEqual:
            constraint = anchor.constraint(lessThanOrEqualTo: toAnchor, constant: padding)
        default:
            fatalError()
        }
        constraint.priority = UILayoutPriority(priority)
        constraint.isActive = true
        return constraint
    }
    
}
