//
//  DimensionConstraintItem.swift
//  
//
//  Created by João Pedro Aragão on 27/12/19.
//  Copyright © 2019 João Pedro Aragão. All rights reserved.
//

import UIKit

struct DimensionConstraintItem {
    
    let anchor: NSLayoutDimension
    
    @discardableResult
    func constraint(toDimension: NSLayoutDimension? = nil,
                    constant: CGFloat = 0,
                    multiplier: CGFloat = 1,
                    relation: NSLayoutConstraint.Relation = .equal,
                    priority: Float = 1000) -> NSLayoutConstraint {
        var constraint: NSLayoutConstraint
        switch relation {
        case .equal:
            if let dimension = toDimension {
                constraint = anchor.constraint(equalTo: dimension, multiplier: multiplier, constant: constant)
            } else {
                constraint = anchor.constraint(equalToConstant: constant)
            }
        case .greaterThanOrEqual:
            if let dimension = toDimension {
                constraint = anchor.constraint(greaterThanOrEqualTo: dimension, multiplier: multiplier, constant: constant)
            } else {
                constraint = anchor.constraint(greaterThanOrEqualToConstant: constant)
            }
        case .lessThanOrEqual:
            if let dimension = toDimension {
                constraint = anchor.constraint(lessThanOrEqualTo: dimension, multiplier: multiplier, constant: constant)
            } else {
                constraint = anchor.constraint(lessThanOrEqualToConstant: constant)
            }
        default:
            fatalError()
        }
        constraint.priority = UILayoutPriority(priority)
        constraint.isActive = true
        return constraint
    }
    
}
