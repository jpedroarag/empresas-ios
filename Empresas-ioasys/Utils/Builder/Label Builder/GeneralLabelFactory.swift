//
//  DetailLabelFactory.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 23/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

import UIKit

struct LabelBuilder: LabelFactory {
    
    let fontWeight: RubikFontWeight
    let size: CGFloat
    let color: UIColor?
    
    func create() -> UILabel {
        let font = UIFont(name: "Rubik-\(fontWeight.rawValue)", size: size)
        let label = UILabel(withFont: font)
        label.tintColor = color
        return label
    }
    
}
