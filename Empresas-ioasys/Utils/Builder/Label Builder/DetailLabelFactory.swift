//
//  DetailLabelFactory.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 23/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

import UIKit

struct ConcreteLabelFactory: LabelFactory {
    
    let fontWeight: RubikFontWeight
    let size: CGFloat
    
    func create() -> UILabel {
        let font = UIFont(name: "Rubik-" + fontWeight.rawValue, size: size)
        return UILabel(withFont: font)
    }
    
}
