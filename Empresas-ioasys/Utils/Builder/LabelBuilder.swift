//
//  DetailLabelFactory.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 23/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

import UIKit

struct LabelBuilder {
    
    private let label = UILabel()
    
    func create() -> UILabel {
        return label
    }
    
    @discardableResult
    func setFontWeight(_ fontWeight: RubikFontWeight) -> LabelBuilder {
        label.font = UIFont(name: "Rubik-\(fontWeight.rawValue)", size: 14)
        return self
    }
    
    @discardableResult
    func setSize(_ size: CGFloat) -> LabelBuilder {
        label.font = label.font.withSize(size)
        return self
    }
    
    @discardableResult
    func setText(_ text: String) -> LabelBuilder {
        label.text = text
        return self
    }
    
    @discardableResult
    func setColor(_ color: UIColor?) -> LabelBuilder {
        label.textColor = color
        return self
    }
    
    @discardableResult
    func setAlignment(_ alignment: NSTextAlignment) -> LabelBuilder {
        label.textAlignment = alignment
        return self
    }
    
    @discardableResult
    func setNumberOfLines(_ lines: Int) -> LabelBuilder {
        label.numberOfLines = lines
        return self
    }
    
    @discardableResult
    func setHidden(_ isHidden: Bool) -> LabelBuilder {
        label.isHidden = isHidden
        return self
    }
    
}
