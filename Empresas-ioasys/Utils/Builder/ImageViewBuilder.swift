//
//  ImageViewBuilder.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 23/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

import UIKit

struct ImageViewBuilder {
    
    private let imageView = UIImageView()
    
    func create() -> UIImageView {
        return imageView
    }
    
    @discardableResult
    func setImage(named name: String) -> ImageViewBuilder {
        imageView.image = UIImage(named: name)
        return self
    }
    
    @discardableResult
    func setSystemImage(named name: String) -> ImageViewBuilder {
        if #available(iOS 13.0, *) {
            imageView.image = UIImage(systemName: name)
        }
        return self
    }
    
    @discardableResult
    func setContentMode(_ contentMode: UIImageView.ContentMode) -> ImageViewBuilder {
        imageView.contentMode = contentMode
        return self
    }
    
    @discardableResult
    func setClipsToBounds(_ clips: Bool) -> ImageViewBuilder {
        imageView.clipsToBounds = clips
        return self
    }
    
    @discardableResult
    func setOverlay(_ layer: CAShapeLayer) -> ImageViewBuilder {
        imageView.layer.mask = layer
        return self
    }
    
    @discardableResult
    func setTintColor(_ color: UIColor?) -> ImageViewBuilder {
        imageView.tintColor = color
        return self
    }
    
    @discardableResult
    func setRadius(_ radius: CGFloat) -> ImageViewBuilder {
        imageView.round(radius: radius)
        return self
    }
    
}
