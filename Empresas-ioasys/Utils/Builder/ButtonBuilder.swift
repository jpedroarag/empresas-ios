//
//  ButtonBuilder.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 24/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

import UIKit

struct ButtonBuilder {
    
    private let button = UIButton()
    
    func create() -> UIButton {
        return button
    }
    
    @discardableResult
    func setTitle(_ title: String) -> ButtonBuilder {
        button.setTitle(title, for: .normal)
        return self
    }
    
    @discardableResult
    func setImage(named name: String) -> ButtonBuilder {
        let image = UIImage(named: name)
        button.setImage(image, for: .normal)
        return self
    }
    
    @discardableResult
    func setSystemImage(named name: String) -> ButtonBuilder {
        if #available(iOS 13.0, *) {
            let image = UIImage(systemName: name)
            button.setImage(image, for: .normal)
        }
        return self
    }
    
    @discardableResult
    func setBackgroundColor(_ color: UIColor?) -> ButtonBuilder {
        button.backgroundColor = color
        return self
    }
    
    @discardableResult
    func setTintColor(_ color: UIColor?) -> ButtonBuilder {
        button.tintColor = color
        return self
    }
    
    @discardableResult
    func setTitleColor(_ color: UIColor?) -> ButtonBuilder {
        button.setTitleColor(color, for: .normal)
        return self
    }
    
    @discardableResult
    func setRadius(_ radius: CGFloat) -> ButtonBuilder {
        button.round(radius: radius)
        return self
    }
    
    @discardableResult
    func setAction(_ target: Any, _ action: Selector) -> ButtonBuilder {
        button.addTarget(target, action: action, for: .touchUpInside)
        return self
    }
    
    @discardableResult
    func setFontWeight(_ fontWeight: RubikFontWeight) -> ButtonBuilder {
        button.titleLabel?.font = UIFont(name: "Rubik-\(fontWeight.rawValue)", size: 14)
        return self
    }
    
    @discardableResult
    func setFontSize(_ size: CGFloat) -> ButtonBuilder {
        button.titleLabel?.font = button.titleLabel?.font.withSize(size)
        return self
    }
    
}
