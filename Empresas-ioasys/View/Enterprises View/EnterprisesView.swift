//
//  EnterprisesView.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 25/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

import UIKit

class EnterprisesView: UIView {
    
    lazy var loadingIndicatorView = LoadingIndicatorView(width: 64)
    
    lazy var headerView = EnterprisesHeaderView()
    lazy var enterprisesTableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.register(EnterpriseTableViewCell.self, forCellReuseIdentifier: "enterprise")
        tableView.register(ResultsFoundTableViewHeader.self, forHeaderFooterViewReuseIdentifier: "results")
        tableView.separatorStyle = .none
        tableView.backgroundColor = .white
        return tableView
    }()
    lazy var noResultsFoundLabel = LabelBuilder()
                                    .setFontWeight(.light)
                                    .setSize(18)
                                    .setColor(.appGray)
                                    .setText("Nenhum resultado encontrado")
                                    .setHidden(true)
                                    .create()
    
    var headerViewHeightConstraint: NSLayoutConstraint?

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        addSubviews(enterprisesTableView, headerView, noResultsFoundLabel, loadingIndicatorView)
        makeConstraints()
    }

    func makeConstraints() {
        headerView.makeConstraints {
            $0.top.constraint(toAnchor: topAnchor, padding: -20)
            $0.left.constraint(toAnchor: leftAnchor)
            $0.right.constraint(toAnchor: rightAnchor)
            headerViewHeightConstraint = $0.height.constraint(toDimension: heightAnchor,
                                                              multiplier: 0.3)
        }
        enterprisesTableView.makeConstraints {
            $0.top.constraint(toAnchor: headerView.backgroundImageView.bottomAnchor)
            $0.left.constraint(toAnchor: leftAnchor)
            $0.right.constraint(toAnchor: rightAnchor)
            $0.bottom.constraint(toAnchor: bottomAnchor)
        }
        noResultsFoundLabel.makeConstraints {
            $0.fill(onView: enterprisesTableView)
        }
        loadingIndicatorView.makeConstraints {
            $0.fill(onView: enterprisesTableView)
        }
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if headerView.searchTextField.isEditing {
           guard let location = touches.first?.location(in: self) else {
               return
           }
           if !headerView.searchTextField.frame.contains(location) {
               headerView.searchTextField.endEditing(true)
           }
       }
    }

    func setHeaderViewHeightMultiplier(_ multiplier: CGFloat) {
       if let constraint = headerViewHeightConstraint {
           constraint.isActive = false
           headerView.removeConstraint(constraint)
           headerViewHeightConstraint = headerView.heightAnchor.constraint(equalTo: heightAnchor,
                                                                           multiplier: multiplier)
           headerViewHeightConstraint!.isActive = true
       }
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
}
