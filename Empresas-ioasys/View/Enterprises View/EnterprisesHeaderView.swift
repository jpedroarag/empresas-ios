//
//  EnterprisesHeaderView.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 24/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

import UIKit

class EnterprisesHeaderView: UIView {

    let searchTextField = SearchTextField()
    let backgroundImageView = ImageViewBuilder()
                                .setImage(named: "BlurredHeaderBackground")
                                .setContentMode(.scaleAspectFill)
                                .setClipsToBounds(true)
                                .create()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubviews(backgroundImageView, searchTextField)
        makeConstraints()
    }
    
    func makeConstraints() {
        let fieldHeight: CGFloat = 48
        backgroundImageView.makeConstraints {
            $0.centerX.constraint(toAnchor: centerXAnchor)
            $0.centerY.constraint(toAnchor: centerYAnchor)
            $0.width.constraint(toDimension: widthAnchor)
            $0.height.constraint(toDimension: heightAnchor, constant: -fieldHeight/2)
        }
        searchTextField.makeConstraints() {
            $0.left.constraint(toAnchor: leftAnchor, padding: 16)
            $0.right.constraint(toAnchor: rightAnchor, padding: -16)
            $0.centerY.constraint(toAnchor: backgroundImageView.bottomAnchor)
            $0.height.constraint(constant: fieldHeight)
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
}
