//
//  ResultsFoundTableViewHeader.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 26/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

import UIKit

class ResultsFoundTableViewHeader: UITableViewHeaderFooterView {

    let label = LabelBuilder()
                    .setFontWeight(.light)
                    .setSize(16)
                    .create()
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        addSubview(label)
        makeConstraints()
    }
    
    func makeConstraints() {
        label.makeConstraints {
            $0.top.constraint(toAnchor: centerYAnchor)
            $0.left.constraint(toAnchor: leftAnchor, padding: 16)
            $0.right.constraint(toAnchor: rightAnchor, padding: -16)
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

}
