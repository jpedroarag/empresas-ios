//
//  EnterpriseImageView.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 26/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

import UIKit

class EnterpriseImageView: UIImageView {

    let logoImageView = ImageViewBuilder()
                        .setContentMode(.scaleAspectFit)
                        .setImage(named: "EnterpriseLogo")
                        .create()
    let label = LabelBuilder()
                    .setAlignment(.center)
                    .setColor(.white)
                    .setFontWeight(.bold)
                    .setSize(18)
                    .create()

    init() {
        super.init(frame: .zero)
        contentMode = .scaleAspectFill
        clipsToBounds = true
        round(radius: 4)
        addSubviews(logoImageView, label)
        makeConstraints()
    }
    
    func makeConstraints() {
        logoImageView.makeConstraints {
            $0.centerX.constraint(toAnchor: centerXAnchor)
            $0.centerY.constraint(toAnchor: centerYAnchor)
            $0.width.constraint(toDimension: logoImageView.heightAnchor)
            $0.width.constraint(constant: 48)
        }
        label.makeConstraints {
            $0.top.constraint(toAnchor: logoImageView.bottomAnchor)
            $0.centerX.constraint(toAnchor: centerXAnchor)
            $0.bottom.constraint(toAnchor: bottomAnchor, padding: -16, relation: .lessThanOrEqual)
        }
    }
    
    func setImage(_ image: UIImage) {
        label.isHidden = true
        logoImageView.isHidden = true
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

}
