//
//  EnterpriseTableViewCell.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 24/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

import UIKit

class EnterpriseTableViewCell: UITableViewCell {
    
    let enterpriseImageView = EnterpriseImageView()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        addSubview(enterpriseImageView)
        makeConstraints()
    }
    
    func makeConstraints() {
        enterpriseImageView.makeConstraints {
            $0.top.constraint(toAnchor: topAnchor, padding: 8)
            $0.left.constraint(toAnchor: leftAnchor, padding: 16)
            $0.right.constraint(toAnchor: rightAnchor, padding: -16)
            $0.height.constraint(constant: 120)
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
}
