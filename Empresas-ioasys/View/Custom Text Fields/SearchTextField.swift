//
//  SearchTextFieldFactory.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 23/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

import UIKit

class SearchTextField: AppTextField {
    
    lazy var searchIconImageView = ImageViewBuilder()
                                    .setImage(named: "Search")
                                    .setContentMode(.scaleAspectFit)
                                    .create()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        placeholder = "Pesquise por empresa"
        returnKeyType = .search
        leftView = paddingView(48)
        addSubview(searchIconImageView)
        makeConstraints()
    }
    
    override func makeConstraints() {
        super.makeConstraints()
        if subviews.contains(searchIconImageView) {
            searchIconImageView.makeConstraints {
                $0.left.constraint(toAnchor: leftAnchor, padding: 16)
                $0.top.constraint(toAnchor: topAnchor, padding: 16)
                $0.bottom.constraint(toAnchor: bottomAnchor, padding: -16)
            }
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
}
