//
//  AppTextField.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 24/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

import UIKit

class AppTextField: UITextField {
    
    lazy var errorIconImageView = ImageViewBuilder()
                                    .setImage(named: "XMarkFilledRed")
                                    .setContentMode(.scaleAspectFit)
                                    .create()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .appWhite
        tintColor = .appPink
        textColor = .black
        
        borderStyle = .none
        errorIconImageView.isHidden = true
        layer.borderColor = UIColor.clear.cgColor
        
        leftView = paddingView(12)
        leftViewMode = .always
        
        layer.borderWidth = 1
        round(radius: 4)
        
        addSubviews(errorIconImageView)
        makeConstraints()
    }
    
    func makeConstraints() {
        errorIconImageView.makeConstraints {
            $0.top.constraint(toAnchor: topAnchor, padding: 16)
            $0.right.constraint(toAnchor: rightAnchor, padding: -16)
            $0.bottom.constraint(toAnchor: bottomAnchor, padding: -16)
        }
    }
    
    func addErrorBorder() {
        layer.borderColor = UIColor.appRed?.cgColor
        errorIconImageView.isHidden = false
    }
    
    func removeErrorBorder() {
        layer.borderColor = UIColor.clear.cgColor
        errorIconImageView.isHidden = true
    }
    
    func paddingView(_ paddingWidth: CGFloat) -> UIView {
        let paddingRect = CGRect(x: 0, y: 0, width: paddingWidth, height: 48)
        let paddingView = UIView(frame: paddingRect)
        return paddingView
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
}
