//
//  PasswordTextFieldFactory.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 23/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

import UIKit

class PasswordTextField: AppTextField {
    
    var shouldMaskPassword = true {
        didSet {
            shouldMaskPassword ? mask() : unmask()
        }
    }
    
    lazy var eyeButton = ButtonBuilder()
                            .setImage(named: "EyeFilled")
                            .setTintColor(.appGray)
                            .setAction(self, #selector(eyeButtonTapped(_:)))
                            .create()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        isSecureTextEntry = true
        addSubview(eyeButton)
        makeConstraints()
    }
    
    override func makeConstraints() {
        super.makeConstraints()
        if subviews.contains(eyeButton) {
            eyeButton.makeConstraints {
                $0.top.constraint(toAnchor: topAnchor, padding: 16)
                $0.right.constraint(toAnchor: rightAnchor, padding: -16)
                $0.bottom.constraint(toAnchor: bottomAnchor, padding: -16)
            }
        }
    }
    
    override func addErrorBorder() {
        super.addErrorBorder()
        eyeButton.isHidden = true
    }
    
    override func removeErrorBorder() {
        super.removeErrorBorder()
        eyeButton.isHidden = false
    }
    
    @objc func eyeButtonTapped(_ button: UIButton) {
        shouldMaskPassword = !shouldMaskPassword
    }
    
    private func mask() {
        isSecureTextEntry = true
    }

    private func unmask() {
        isSecureTextEntry = false
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
}
