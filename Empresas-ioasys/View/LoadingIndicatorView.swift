//
//  LoadingIndicatorView.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 25/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

import UIKit

class LoadingIndicatorView: UIView {
    
    enum AnimationDirection {
        case forwards
        case backwards
    }
    
    private lazy var outterAnimationLayer = CALayer()
    private lazy var innerAnimationLayer = CALayer()
    private lazy var layerContainerView = UIView()
       
    lazy var isAnimating = false
    lazy var hidesWhenStopped = true
    
    lazy var innerImage = UIImage(named: "InnerEllipse")
    lazy var outterImage = UIImage(named: "OutterEllipse")
    
    init(width: CGFloat) {
        super.init(frame: .zero)

        let outterSize = CGSize(width: width, height: width)
        let innerSize = CGSize(width: width - 24, height: width - 24)
        
        outterAnimationLayer.frame = CGRect(origin: .zero, size: outterSize)
        outterAnimationLayer.contents = outterImage?.cgImage
        outterAnimationLayer.masksToBounds = true
        
        innerAnimationLayer.frame = CGRect(origin: CGPoint(x: 12, y: 12), size: innerSize)
        innerAnimationLayer.contents = innerImage?.cgImage
        innerAnimationLayer.masksToBounds = true
        
        layerContainerView.frame = CGRect(origin: .zero, size: outterSize)
        layerContainerView.layer.addSublayer(innerAnimationLayer)
        layerContainerView.layer.addSublayer(outterAnimationLayer)
        addSubview(layerContainerView)
        
        makeConstraints(width: width)
        configureAnimations()
        
        isHidden = true
        backgroundColor = .clear
    }
    
    private func makeConstraints(width: CGFloat) {
        layerContainerView.makeConstraints {
            $0.centerY.constraint(toAnchor: centerYAnchor)
            $0.centerX.constraint(toAnchor: centerXAnchor)
            $0.width.constraint(constant: width)
            $0.height.constraint(constant: width)
        }
    }
    
    private func configureAnimations() {
        addRotation(forLayer: innerAnimationLayer, direction: .forwards)
        addRotation(forLayer: outterAnimationLayer, direction: .backwards)
        pause(layer: innerAnimationLayer)
        pause(layer: outterAnimationLayer)
    }
    
    private func addRotation(forLayer layer: CALayer, direction: AnimationDirection) {
        let initialValue = NSNumber(value: 0.0)
        let finalValue = NSNumber(value: 2 * Double.pi)
        let rotation = CABasicAnimation(keyPath:"transform.rotation.z")
        rotation.duration = 1.0
        rotation.isRemovedOnCompletion = false
        rotation.repeatCount = HUGE
        rotation.fillMode = .forwards
        rotation.fromValue = (direction == .forwards) ? initialValue : finalValue
        rotation.toValue = (direction == .forwards) ? finalValue : initialValue
        layer.add(rotation, forKey: "rotate")
    }

    private func pause(layer: CALayer) {
        let pausedTime = layer.convertTime(CACurrentMediaTime(), from: nil)
        layer.speed = 0.0
        layer.timeOffset = pausedTime
        isAnimating = false
    }

    private func resume(layer: CALayer) {
        let pausedTime = layer.timeOffset
        layer.speed = 1.0
        layer.timeOffset = 0.0
        layer.beginTime = 0.0
        
        let timeSincePause = layer.convertTime(CACurrentMediaTime(), from: nil) - pausedTime
        layer.beginTime = timeSincePause
        isAnimating = true
    }

    func startAnimating() {
        if isAnimating {
            return
        }
        if hidesWhenStopped {
            isHidden = false
        }
        resume(layer: innerAnimationLayer)
        resume(layer: outterAnimationLayer)
    }

    func stopAnimating() {
        if hidesWhenStopped {
            isHidden = true
        }
        pause(layer: innerAnimationLayer)
        pause(layer: outterAnimationLayer)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
