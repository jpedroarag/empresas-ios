//
//  LoginInputsView.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 23/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

import UIKit

class LoginInputsView: UIView {
    
    lazy var emailDetailLabel = LabelBuilder()
                                    .setFontWeight(.regular)
                                    .setSize(14)
                                    .setColor(.appGray)
                                    .setText("Email")
                                    .create()

    lazy var emailTextField = AppTextField()
    
    lazy var passwordDetailLabel = LabelBuilder()
                                    .setFontWeight(.regular)
                                    .setSize(14)
                                    .setColor(.appGray)
                                    .setText("Senha")
                                    .create()
    lazy var passwordTextField = PasswordTextField()
    
    lazy var uncorrectCredentialsLabel = LabelBuilder()
                                        .setFontWeight(.light)
                                        .setSize(12)
                                        .setColor(.appRed)
                                        .setText("Credenciais incorretas")
                                        .setAlignment(.right)
                                        .create()
    
    var passwordTextFieldReturnAction: ((UITextField) -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        uncorrectCredentialsLabel.isHidden = true
        emailTextField.delegate = self
        passwordTextField.delegate = self
        passwordTextField.returnKeyType = .go
        emailTextField.returnKeyType = .next
        emailTextField.keyboardType = .emailAddress
        addSubviews(emailDetailLabel, emailTextField,
                    passwordDetailLabel, passwordTextField,
                    uncorrectCredentialsLabel)
        makeConstraints()
    }
    
    func makeConstraints() {
        emailDetailLabel.makeConstraints {
            $0.top.constraint(toAnchor: topAnchor)
            $0.left.constraint(toAnchor: leftAnchor)
        }
        emailTextField.makeConstraints {
            $0.top.constraint(toAnchor: emailDetailLabel.bottomAnchor, padding: 4)
            $0.left.constraint(toAnchor: emailDetailLabel.leftAnchor, padding: -4)
            $0.right.constraint(toAnchor: rightAnchor, padding: 4)
            $0.height.constraint(constant: 48)
        }
        passwordDetailLabel.makeConstraints {
            $0.top.constraint(toAnchor: emailTextField.bottomAnchor, padding: 16)
            $0.left.constraint(toAnchor: emailDetailLabel.leftAnchor)
        }
        passwordTextField.makeConstraints {
            $0.top.constraint(toAnchor: passwordDetailLabel.bottomAnchor, padding: 4)
            $0.left.constraint(toAnchor: passwordDetailLabel.leftAnchor, padding: -4)
            $0.right.constraint(toAnchor: emailTextField.rightAnchor)
            $0.height.constraint(toDimension: emailTextField.heightAnchor)
        }
        uncorrectCredentialsLabel.makeConstraints {
            $0.right.constraint(toAnchor: rightAnchor)
            $0.top.constraint(toAnchor: passwordTextField.bottomAnchor, padding: 4)
        }
    }
    
    func addIncorrectCredentialsOverlays() {
        emailTextField.addErrorBorder()
        passwordTextField.addErrorBorder()
        uncorrectCredentialsLabel.isHidden = false
    }
    
    func removeIncorrectCredentialsOverlays() {
        emailTextField.removeErrorBorder()
        passwordTextField.removeErrorBorder()
        uncorrectCredentialsLabel.isHidden = true
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
}

extension LoginInputsView: UITextFieldDelegate {
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        removeIncorrectCredentialsOverlays()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField === emailTextField {
            passwordTextField.becomeFirstResponder()
        }
        if textField === passwordTextField {
            passwordTextField.resignFirstResponder()
            passwordTextFieldReturnAction?(passwordTextField)
        }
        return true
    }
    
}
