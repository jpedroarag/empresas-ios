//
//  LoginView.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 25/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

import UIKit

class LoginView: UIView {

    lazy var loadingIndicatorView = LoadingIndicatorView(width: 64)
    
    lazy var headerView = LoginHeaderView()
    lazy var inputsView = LoginInputsView()
    lazy var loginButton = ButtonBuilder()
                            .setBackgroundColor(.appPink)
                            .setTitle("ENTRAR")
                            .setRadius(8)
                            .setFontWeight(.medium)
                            .setFontSize(16)
                            .setTitleColor(.white)
                            .setAction(self, #selector(loginButtonTapped(_:)))
                            .create()
    
    var headerViewHeightConstraint: NSLayoutConstraint?
    var buttonAction: ((UIButton) -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        loadingIndicatorView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        addSubviews(headerView, inputsView, loginButton, loadingIndicatorView)
        makeConstraints()
    }
    
    func makeConstraints() {
        headerView.makeConstraints {
            $0.top.constraint(toAnchor: topAnchor)
            $0.left.constraint(toAnchor: leftAnchor)
            $0.right.constraint(toAnchor: rightAnchor)
            headerViewHeightConstraint = $0.height.constraint(toDimension: heightAnchor,
                                                              multiplier: 0.4)
        }
        inputsView.makeConstraints {
            $0.top.constraint(toAnchor: headerView.bottomAnchor)
            $0.left.constraint(toAnchor: leftAnchor, padding: 20)
            $0.right.constraint(toAnchor: rightAnchor, padding: -20)
            $0.bottom.constraint(toAnchor: bottomAnchor)
        }
        loginButton.makeConstraints {
            $0.top.constraint(toAnchor: inputsView.passwordTextField.bottomAnchor, padding: 40)
            $0.left.constraint(toAnchor: leftAnchor, padding: 30)
            $0.right.constraint(toAnchor: rightAnchor, padding: -30)
            $0.height.constraint(toDimension: inputsView.passwordTextField.heightAnchor)
        }
        loadingIndicatorView.makeConstraints {
            $0.fill(onView: self)
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let usernameField = inputsView.emailTextField
        let passwordField = inputsView.passwordTextField
        
        if usernameField.isEditing || passwordField.isEditing {
            guard let location = touches.first?.location(in: self) else {
                return
            }
            if !usernameField.frame.contains(location) {
                usernameField.endEditing(true)
            }
            if !passwordField.frame.contains(location) {
                passwordField.endEditing(true)
            }
        }
        
    }
    
    func setHeaderViewHeightMultiplier(_ multiplier: CGFloat) {
        if let constraint = headerViewHeightConstraint {
            constraint.isActive = false
            headerView.removeConstraint(constraint)
            headerViewHeightConstraint = headerView.heightAnchor.constraint(equalTo: heightAnchor,
                                                                            multiplier: multiplier)
            headerViewHeightConstraint!.isActive = true
        }
    }
    
    @objc func loginButtonTapped(_ button: UIButton) {
        buttonAction?(button)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

}
