//
//  LoginHeaderView.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 23/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

import UIKit

class LoginHeaderView: UIView {
    
    lazy var backgroundImageView = ImageViewBuilder()
                                            .setImage(named: "BlurredHeaderBackground")
                                            .setContentMode(.scaleAspectFill)
                                            .setClipsToBounds(true)
                                            .create()
    
    lazy var logoImageView = ImageViewBuilder()
                                .setImage(named: "LogoHome")
                                .setContentMode(.scaleAspectFit)
                                .create()
    
    lazy var headerLabel = LabelBuilder()
                            .setFontWeight(.regular)
                            .setSize(20)
                            .setColor(.white)
                            .setText("Seja bem-vindo ao empresas!")
                            .create()
    
    private lazy var curveStartingCoordinate: CGFloat = 0.0
    private var logoCenterYConstraint: NSLayoutConstraint?
    private var isExpanded: Bool = true
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        clipsToBounds = false
        addSubviews(backgroundImageView, logoImageView, headerLabel)
        makeConstraints()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        curveStartingCoordinate = 3.5 * bounds.height/5
        logoCenterYConstraint?.constant = isExpanded ? (curveStartingCoordinate - bounds.height)/3 : 0
        setCircularMask()
    }
    
    func setCircularMask() {
        let leftUpper = CGPoint.zero
        let rightUpper = CGPoint(x: bounds.width, y: 0)
        let leftBottom = CGPoint(x: 0, y: curveStartingCoordinate)
        let rightBottom = CGPoint(x: bounds.width, y: curveStartingCoordinate)
        let middleBottom = CGPoint(x: bounds.width/2, y: bounds.height)
        
        let midpointLeft = CGPoint.midpoint(left: leftBottom, right: middleBottom)
        let midpointRight = CGPoint.midpoint(left: middleBottom, right: rightBottom)
        
        let path = UIBezierPath()
        path.move(to: leftUpper)
        path.addLine(to: leftBottom)
        path.addCurve(to: rightBottom,
                      controlPoint1: CGPoint.midpoint(left: midpointLeft, right: middleBottom),
                      controlPoint2: CGPoint.midpoint(left: middleBottom, right: midpointRight))
        path.addLine(to: rightUpper)
        path.close()
        
        let shape = CAShapeLayer()
        shape.path = path.cgPath
        backgroundImageView.layer.mask = shape
    }
    
    func makeConstraints() {
        backgroundImageView.makeConstraints {
            $0.fill(onView: self)
        }
        logoImageView.makeConstraints {
            $0.centerX.constraint(toAnchor: centerXAnchor)
            logoCenterYConstraint = $0.centerY.constraint(toAnchor: centerYAnchor)
        }
        headerLabel.makeConstraints {
            $0.centerX.constraint(toAnchor: centerXAnchor)
            $0.top.constraint(toAnchor: logoImageView.bottomAnchor, padding: 16)
        }
    }
    
    func expand() {
        headerLabel.isHidden = false
        isExpanded = true
    }
    
    func collapse() {
        headerLabel.isHidden = true
        isExpanded = false
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
}
