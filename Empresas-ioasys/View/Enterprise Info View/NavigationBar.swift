//
//  NavigationBar.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 24/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

import UIKit

class NavigationBar: UIView {

    lazy var titleLabel = LabelBuilder()
                            .setFontWeight(.medium)
                            .setColor(.black)
                            .setSize(20)
                            .setAlignment(.center)
                            .create()
    
    lazy var backButton = ButtonBuilder()
                            .setSystemImage(named: "arrow.left")
                            .setTintColor(.appPink)
                            .setBackgroundColor(.appWhite)
                            .setRadius(4)
                            .setAction(self, #selector(backButtonTapped(_:)))
                            .create()
    var buttonAction: ((UIButton) -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubviews(backButton, titleLabel)
        makeConstraints()
    }
    
    func makeConstraints() {
        titleLabel.makeConstraints {
            $0.centerX.constraint(toAnchor: centerXAnchor)
            $0.centerY.constraint(toAnchor: centerYAnchor)
        }
        backButton.makeConstraints {
            $0.left.constraint(toAnchor: leftAnchor, padding: 16)
            $0.centerY.constraint(toAnchor: centerYAnchor)
            $0.height.constraint(constant: 40)
            $0.width.constraint(toDimension: backButton.heightAnchor)
        }
    }
    
    @objc func backButtonTapped(_ button: UIButton) {
        buttonAction?(button)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

}
