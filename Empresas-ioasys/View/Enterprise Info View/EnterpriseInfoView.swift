//
//  EnterpriseInfoView.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 25/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

import UIKit

class EnterpriseInfoView: UIView {
    
    private lazy var labelContainerScrollView = UIScrollView()
    
    lazy var navigationBar = NavigationBar()
    lazy var imageView = EnterpriseImageView()
    lazy var infoLabel = LabelBuilder()
                            .setFontWeight(.light)
                            .setSize(18)
                            .setColor(.black)
                            .setNumberOfLines(0)
                            .create()

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        addSubviews(navigationBar, imageView, labelContainerScrollView)
        labelContainerScrollView.addSubview(infoLabel)
        makeConstraints()
    }
    
    func makeConstraints() {
        navigationBar.makeConstraints() {
            $0.top.constraint(toAnchor: safeAreaLayoutGuide.topAnchor)
            $0.left.constraint(toAnchor: leftAnchor)
            $0.right.constraint(toAnchor: rightAnchor)
            $0.height.constraint(constant: 64)
        }
        imageView.makeConstraints {
            $0.top.constraint(toAnchor: navigationBar.bottomAnchor)
            $0.left.constraint(toAnchor: leftAnchor)
            $0.right.constraint(toAnchor: rightAnchor)
            $0.height.constraint(constant: 128)
        }
        labelContainerScrollView.makeConstraints {
            $0.top.constraint(toAnchor: imageView.bottomAnchor)
            $0.left.constraint(toAnchor: leftAnchor)
            $0.right.constraint(toAnchor: rightAnchor)
            $0.bottom.constraint(toAnchor: bottomAnchor)
        }
        infoLabel.makeConstraints {
            $0.top.constraint(toAnchor: labelContainerScrollView.topAnchor, padding: 24)
            $0.bottom.constraint(toAnchor: labelContainerScrollView.bottomAnchor)
            $0.left.constraint(toAnchor: labelContainerScrollView.leftAnchor, padding: 16)
            $0.width.constraint(toDimension: widthAnchor, constant: -16)
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
}
