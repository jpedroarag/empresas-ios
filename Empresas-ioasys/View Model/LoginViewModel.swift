//
//  LoginViewModel.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 25/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

import Foundation

class LoginViewModel {
    
    private let provider = URLSessionProvider()
    lazy var isLoggedIn = Box(false)
    
    func login(email: String, password: String) {
        let user = User(email: email, password: password)
        provider.request(.post(user)) { [weak self] result, headerFields in
            guard let self = self else { return }
            switch result {
            case .success(_):
                if let client = headerFields?["client"] as? String,
                    let uid = headerFields?["uid"] as? String,
                    let token = headerFields?["access-token"] as? String {
                    UserDefaults.standard.login(client: client, uid: uid, token: token)
                }
                self.isLoggedIn.value = true
            case .failure(let error):
                print(error.localizedDescription)
                self.isLoggedIn.value = false
            }
        }
    }
    
}
