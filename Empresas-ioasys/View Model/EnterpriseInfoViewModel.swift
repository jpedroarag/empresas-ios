//
//  EnterpriseInfoViewModel.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 25/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

import UIKit

class EnterpriseInfoViewModel {
    
    let enterprise: Enterprise
    
    init(enterprise: Enterprise) {
        self.enterprise = enterprise
    }
    
    func populate(_ view: EnterpriseInfoView) {
        view.navigationBar.titleLabel.text = enterprise.enterpriseName
        view.imageView.label.text = enterprise.enterpriseName
        view.infoLabel.text = enterprise.description
    }
    
}
