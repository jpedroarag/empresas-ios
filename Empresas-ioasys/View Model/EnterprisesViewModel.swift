//
//  EnterprisesViewModel.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 25/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

import UIKit

class EnterprisesViewModel: NSObject {
    
    private let provider: URLSessionProvider
    private var allEnterprises: [Enterprise]
    
    var visibleEnterprises: Box<[Enterprise]>
    var isSearching: Bool {
        return allEnterprises.count != visibleEnterprises.value.count
    }
    
    override init() {
        self.provider = URLSessionProvider()
        self.allEnterprises = []
        self.visibleEnterprises = Box([])
    }
    
    func fetchEnterprises() {
        provider.request(.get(Enterprise.self)) { [weak self] result, _ in
            guard let self = self else { return }
            switch result {
            case .success(let item):
                self.allEnterprises = item.results ?? []
                self.visibleEnterprises.value = self.allEnterprises
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    func search(for enterpriseName: String) -> [Enterprise] {
        let lowerCasedSearch = enterpriseName.lowercased()
        return allEnterprises.filter {
            $0.enterpriseName?.lowercased().contains(lowerCasedSearch) ?? false
        }
    }
    
}

extension EnterprisesViewModel: UITextFieldDelegate {
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if textField.text == "" {
            visibleEnterprises.value = allEnterprises
        } else {
            visibleEnterprises.value = search(for: textField.text!)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard textField.text != "" else {
            return false
        }
        visibleEnterprises.value = search(for: textField.text!)
        return true
    }
    
}
