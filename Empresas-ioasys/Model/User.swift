//
//  User.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 25/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

import Foundation

struct User: CustomCodable {
    static var path: String = "/api/v1/users/auth/sign_in"
    static var headerKey: String? = nil
    
    let email: String
    let password: String
}
