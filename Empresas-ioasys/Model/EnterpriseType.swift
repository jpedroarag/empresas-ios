//
//  EnterpriseType.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 25/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

import Foundation

struct EnterpriseType: Codable {
    
    public let id: Int
    public let enterpriseTypeName: String
    
}
