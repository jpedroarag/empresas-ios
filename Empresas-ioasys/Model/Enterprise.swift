//
//  Enterprise.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 25/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

import Foundation

struct Enterprise: CustomCodable {
    static var path: String = "/api/v1/enterprises"
    static var headerKey: String? = "enterprises"
    
    public let city: String?
    public let country: String?
    public let description: String?
    public let emailEnterprise: String?
    public let enterpriseName: String?
    public let enterpriseType: EnterpriseType?
    public let facebook: String?
    public let id: Int?
    public let linkedin: String?
    public let ownEnterprise: Bool?
    public let phone: String?
    public let photo: String?
    public let sharePrice: Int?
    public let twitter: String?
    public let value: Int?
}
