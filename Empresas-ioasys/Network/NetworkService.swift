//
//  GarageService.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 25/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

import Foundation

public enum NetworkService<T: CustomCodable>: Service {
    public typealias CustomType = T
    
    case get(T.Type, id: Int? = nil)
    case post(T)
    case update(T)
    
    public var baseURL: URL {
        let url = "https://empresas.ioasys.com.br"
        return URL(string: url) ?? URL(fileURLWithPath: "")
    }

    public var path: String {
        switch self {
        case .get(let type, let id):
            if let id = id {
                return "\(type.path)\(id)"
            } else {
                return type.path
            }            
        case .post(let item), .update(let item):
            return type(of: item).path
        }
    }
    
    public var method: HTTPMethod {
        switch self {
        case .get:
            return .get
        case .post:
            return .post
        case .update:
            return .patch
        }
    }
    
    public var task: Task {
        switch self {
        case .get:
            return .requestPlain
        case .post(let item), .update(let item):
            return .requestWithBody(item)
        }
    }
    
    public var headers: Headers? {
        switch self {
        case .get(let item, _):
            switch item {
            case is Enterprise.Type:
                return ["Content-type": "application/json",
                        "client": UserDefaults.client,
                        "uid": UserDefaults.uid,
                        "access-token": UserDefaults.token]
            default:
                return nil
            }
        case .post(_):
            return ["Content-type": "application/json"]
        case .update:
            return ["Content-type": "application/json"]
        }
    }
    
    public var parametersEncoding: ParametersEncoding {
        switch self {
        case .get:
            return .url
        case .post, .update:
            return .json
        }
    }
}

public extension URLSessionProvider {
    func request<T: Decodable>(_ service: NetworkService<T>,
                               completion: @escaping (Result<Response<T>, Error>,
                                                     [AnyHashable: Any]?) -> Void) {
        
        self.request(service: service, completion: completion)
    }
}
