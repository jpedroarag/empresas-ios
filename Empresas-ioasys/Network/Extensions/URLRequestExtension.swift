//
//  URLRequestExtension.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 25/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

import Foundation

extension URLRequest {
    
    init<G: Service>(service: G) {
        if let urlComponents = URLComponents(service: service),
            let url = urlComponents.url {
            self.init(url: url)

            self.httpMethod = service.method.rawValue
            
            service.headers?.forEach { key, value in
                addValue(value, forHTTPHeaderField: key)
            }
            
            guard case let .requestWithBody(payload) = service.task,
                service.parametersEncoding == .json else {
                    return
            }
            
            self.httpBody = payload.data
        } else {
            self.init(url: URL(fileURLWithPath: ""))
        }
    }
}
