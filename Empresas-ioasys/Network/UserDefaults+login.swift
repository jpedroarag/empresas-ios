//
//  UserDefaults+Login.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 25/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

import Foundation

extension UserDefaults {
    
    static var token: String {
        return UserDefaults.standard.string(forKey: "access-token") ?? ""
    }
    
    static func set(token: String) {
        UserDefaults.standard.set(token, forKey: "access-token")
    }
    
    static var uid: String {
        return UserDefaults.standard.string(forKey: "uid") ?? ""
    }
    
    static func set(uid: String) {
        UserDefaults.standard.set(uid, forKey: "uid")
    }
    
    static var client: String {
        return UserDefaults.standard.string(forKey: "client") ?? ""
    }
    
    static func set(client: String) {
        UserDefaults.standard.set(client, forKey: "client")
    }
    
    func login(client: String, uid: String, token: String) {
        UserDefaults.set(client: client)
        UserDefaults.set(uid: uid)
        UserDefaults.set(token: token)
    }
    
    func logout() {
        UserDefaults.set(client: "")
        UserDefaults.set(uid: "")
        UserDefaults.set(token: "")
    }
    
}
