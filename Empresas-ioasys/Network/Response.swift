//
//  Result.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 25/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

import Foundation

public struct Response<T: CustomCodable>: Codable {
    public let result: T?
    public let results: [T]?
    public let status: String?
    public let notice: String?
    public let jwt: String?
}
