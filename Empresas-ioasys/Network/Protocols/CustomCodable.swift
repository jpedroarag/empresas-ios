//
//  CustomCodable.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 25/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

public protocol CustomCodable: Codable {
    static var path: String {get}
    static var headerKey: String? { get }
}
