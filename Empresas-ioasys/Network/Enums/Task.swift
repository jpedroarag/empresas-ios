//
//  Task.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 25/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

public enum Task {
    case requestPlain
    case requestParameters([String: Any])
    case requestWithBody(CustomCodable)
}
