//
//  ParametersEncoding.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 25/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

public enum ParametersEncoding {
    case url
    case json
}
