//
//  URLSessionProvider.swift
//  Empresas-ioasys
//
//  Created by João Pedro Aragão on 25/05/20.
//  Copyright © 2020 João Pedro Aragão. All rights reserved.
//

import Foundation

public final class URLSessionProvider {
    
    private var session: URLSession
    
    public init(session: URLSession = URLSession.shared) {
        self.session = session
    }
    
    public func request<G: Service>(service: G,
                                    completion: @escaping (Result<Response<G.CustomType>, Error>,
                                                          [AnyHashable: Any]?) -> Void) {
        
        let request = URLRequest(service: service)
        session.dataTask(with: request) { (result) in
            self.handleResult(result: result, completion: completion)
        }.resume()
        
    }
    
    private func handleResult<T: Decodable>(result: Result<(URLResponse, Data), Error>,
                                            completion: (Result<Response<T>, Error>,
                                                        [AnyHashable: Any]?) -> Void) {
        switch result {
        case .failure(let error):
            print("\n\n\nerror\(error)\n\n\n")
            completion(.failure(error), nil)
        case .success(let response, let data):
            guard let httpResponse = response as? HTTPURLResponse else {
                return completion(.failure(NetworkError.noJSONData), nil)
            }
            guard let dataString = String(bytes: data, encoding: .utf8) else { return }
            switch httpResponse.statusCode {
            case 200...299:
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase

                do {
                    let decodingData = dataRemovingHeaderKey(from: data, type: T.self)
                    let model = try decoder.decode(Response<T>.self, from: decodingData)
                    if let notice = model.notice {
                        completion(.failure(NetworkError.noticeError(notice)), httpResponse.allHeaderFields)
                    } else {
                        completion(.success(model), httpResponse.allHeaderFields)
                    }

                } catch {
                    print("DECODE ERROR: \(error)")
                    completion(.failure(NetworkError.decodeError(error.localizedDescription)), httpResponse.allHeaderFields)
                }
                
            case 400...499:
                completion(.failure(NetworkError.clientError(statusCode: httpResponse.statusCode, dataResponse: dataString)), httpResponse.allHeaderFields)
            case 500...599:
                completion(.failure(NetworkError.serverError(statusCode: httpResponse.statusCode, dataResponse: dataString)), httpResponse.allHeaderFields)
            default:
                completion(.failure(NetworkError.unknown), httpResponse.allHeaderFields)
            }
            
        }
        
    }
    
    private func dataRemovingHeaderKey<T: CustomCodable>(from data: Data, type: T.Type) -> Data {
        if let headerKey = T.headerKey,
            let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
            var newJson = json
            newJson["results"] = newJson[headerKey]
            newJson.removeValue(forKey: headerKey)
            if let data = try? JSONSerialization.data(withJSONObject: newJson, options: []) {
                return data
            }
        }
        return data
    }
}
